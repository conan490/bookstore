package com.website.bookStore.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category", catalog = "bookstore")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long categoryID;
	private String cateName;
	private List<Book> books = new ArrayList<>();
	
	public Category(String cateName) {
		this.cateName = cateName;
	}
	
	public Category() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CATEGORY_ID",nullable = false, unique = true)
	public Long getcategoryID() {
		return categoryID;
	}

	public void setcategoryID(Long categoryID) {
		this.categoryID = categoryID;
	}

	@Column(name = "CATEGORY_NAME", length = 50, nullable = false)
	public String getCateName() {
		return cateName;
	}

	public void setCateName(String cateName) {
		this.cateName = cateName;
	}

	@OneToMany(fetch=FetchType.EAGER, mappedBy="category",cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
}
