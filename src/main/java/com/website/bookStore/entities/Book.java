package com.website.bookStore.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "book", catalog = "bookstore")
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long bookID;
	private String bookName;
	private String author;
	private Float price;
	private String status;
	private String describe;
	private  Category category;
	private List<Image> images;

	public Book(String bookName, String author, Float price, String status,String describe) {

		this.bookName = bookName;
		this.author = author;
		this.price = price;
		this.describe = describe;
		this.status = status;
	}

	public Book() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BOOK_ID", unique = true, nullable = false)
	public Long getBookID() {
		return bookID;
	}

	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}

	@Column(name = "BOOK_NAME", nullable = false, length = 70)
	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	@Column(name = "AUTHOR", length = 50, nullable = false)
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Column(name = "PRICE", nullable = false)
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	@Column(name="STATUS",length=50,nullable=false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "TITLE", length = 50, nullable = false)
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CATEGORY_ID", nullable = false)
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="book",cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	
}
