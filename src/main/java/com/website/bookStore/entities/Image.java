package com.website.bookStore.entities;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="image",catalog="bookstore")
public class Image implements Serializable {
		private static final long serialVersionUID = 1L;
		private Long imageID;
		private byte[] imageData;
		private Book book;
		
		public Image(byte[] imageData, Book book) {
			this.imageData = imageData;
			this.book = book;
		}
		
		public Image() {
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "IMAGE_ID", unique = true, nullable = false)
		public Long getImageID() {
			return imageID;
		}

		public void setImageID(Long imageId) {
			this.imageID = imageId;
		}

		@Column(name = "IMAGE_DATA", nullable = false)
		public byte[] getImageData() {
			return imageData;
		}

		public void setImageData(byte[] imageData) {
			this.imageData = imageData;
		}
		
		
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="BOOK_ID", nullable = false)
		public Book getBook() {
			return book;
		}

		public void setBook(Book book) {
			this.book = book;
		}

		@Transient
		public String getBase64() {
			return Base64.getEncoder().encodeToString(imageData);
		}

		public void setBase64(String base64) {
		}
 }
