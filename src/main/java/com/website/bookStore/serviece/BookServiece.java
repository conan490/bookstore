package com.website.bookStore.serviece;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.website.bookStore.DAO.DAO;
import com.website.bookStore.entities.Book;

@Transactional
@Service
public class BookServiece {
	
	@Autowired
	private DAO<Book> bookDAO;
	
	public List<Book> getAll(){
		return bookDAO.getAll();
	}
	
	public Book getById(Long bookId) {
		return bookDAO.getById(bookId);
	}
	
	public Book add(Book book) {
		return bookDAO.add(book);
	}
	
	public Boolean update(Book book) {
		return bookDAO.update(book);
	}
	
	public Boolean delete (Book book) {
		return bookDAO.delete(book);
	}
	
	public Boolean deleteById(Long bookID) {
		return bookDAO.deleteById(bookID);
	}
}
