package com.website.bookStore.serviece;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.website.bookStore.DAO.DAO;
import com.website.bookStore.entities.Category;

@Transactional
@Service
public class CategoryServiece {
	
	@Autowired
	DAO<Category> cateDAO;
	
	public List<Category> getAll(){
		return cateDAO.getAll();
	}
	
	public Category getById(Long categoryId) {
		return cateDAO.getById(categoryId);
	}
	
	public Category add(Category category) {
		return cateDAO.add(category);
	}
	
	public Boolean update(Category category) {
		return cateDAO.update(category);
	}
	
	public Boolean delete(Category category) {
		return cateDAO.delete(category);
	}
	
	public Boolean deleteById(Long categoryID) {
		return cateDAO.deleteById(categoryID);
	}
}
