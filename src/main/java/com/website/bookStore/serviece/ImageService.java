package com.website.bookStore.serviece;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.website.bookStore.DAO.DAO;
import com.website.bookStore.DAO.ImageDAO;
import com.website.bookStore.entities.Image;

@Service
@Transactional
public class ImageService {
	
	@Autowired
	DAO<Image> imageDAO;
	
	@Autowired
	ImageDAO imgDAO;
	
	public List<Image> getAll(){
		return imageDAO.getAll();
	}
	
	public Image getById(Long imageId) {
		return imageDAO.getById(imageId);
	}
	
	public Image add(Image image) {
		return imageDAO.add(image);
	}
	
	public Boolean update(Image image) {
		return imageDAO.update(image);
	}
	
	public Boolean delete (Image image) {
		return imageDAO.delete(image);
	}
	
	public Boolean deleteById(Long imageID) {
		return imageDAO.deleteById(imageID);
	}
	
	public List<Image> getImagesByBookID(Long bookID){
		return imgDAO.getImageByBookID(bookID);
	}
}
