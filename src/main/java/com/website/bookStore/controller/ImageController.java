package com.website.bookStore.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.bookStore.serviece.ImageService;

@Controller

public class ImageController {
	
	@Autowired
	private ImageService imageService;
	
	public ModelAndView getImagesByBookID(@RequestParam Long bookID, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("images",imageService.getImagesByBookID(bookID));
		model.setViewName("image");
		return model;
	}
}
