package com.website.bookStore.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.bookStore.entities.Book;
import com.website.bookStore.entities.Category;
import com.website.bookStore.serviece.BookServiece;
import com.website.bookStore.serviece.CategoryServiece;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryServiece cateServiece;
	
	@Autowired
	private BookServiece bookService;
	
	public ModelAndView getCategories(@PathVariable Map<String,String> pathVariable, HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		model.setViewName("categories");
		model.addObject("categories",cateServiece.getAll());
		return model;
	}
	
	
	public ModelAndView addCategory() {
		ModelAndView model = new ModelAndView();
		model.setViewName("CategoryInfo");
		model.addObject("category",new Category());
		return model;
	}
	
	public String saveCategory(HttpServletRequest request, @ModelAttribute("cate") Category cate) {
		if(cate.getcategoryID() == null) {
			cateServiece.add(cate);
			return "redirect:/admin/category/";
		}else {
			cateServiece.update(cate);
			return "redirect:/admin/category/";
		}
	}
	
	public ModelAndView getCategory(@RequestParam Long categoryID, @RequestParam String mode) {
		ModelAndView model = new ModelAndView();
		model.setViewName("CategoryInfo");
		model.addObject("category",cateServiece.getById(categoryID));
		model.addObject("mode", mode);
		return model;
		
	}
	
	public String deleteCategory(@RequestParam Long categoryID) {
		Category category =cateServiece.getById(categoryID);
		List<Book> books = category.getBooks();
		for(Book book : books) {
			bookService.delete(book);
		}
		cateServiece.delete(category);
		return "redirect:/admin/category/";
	}
}
