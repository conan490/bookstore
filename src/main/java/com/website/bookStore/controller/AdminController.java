package com.website.bookStore.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.website.bookStore.entities.Book;
import com.website.bookStore.entities.Category;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	BookController bookController;
	
	@Autowired
	CategoryController categoryController;
	
	@Autowired
	ImageController imageController;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String admin() {
		return "admin";
	}
	
	//Book
	@RequestMapping(value = {"/book"}, method = RequestMethod.GET)
	public ModelAndView getBooks(@PathVariable Map<String,String> pathVariable, HttpServletRequest request ) {
		return bookController.getBooks(pathVariable, request);
		
	}
	
	@RequestMapping(value = {"/book/addBook"}, method = RequestMethod.GET)
	public ModelAndView addBook() {
		return bookController.addBook();
	}
	
	@RequestMapping(value="/book/save")
	public String saveBook(HttpServletRequest request, @ModelAttribute("book") Book book,@RequestParam CommonsMultipartFile[] imageUpload) {		
		return bookController.saveBook(request, book,imageUpload);
	}
	
	@RequestMapping(value="/book/getBook",method = RequestMethod.GET)
	public ModelAndView getBook(@RequestParam Long bookID,@RequestParam String mode) {
		return bookController.getBook(bookID, mode);
	}
	
	@RequestMapping(value="/book/delete")
	public String deleteBook(@RequestParam Long bookID) {
		return bookController.deleteBook(bookID);
	}
	
	@RequestMapping(value="/book/getImages", method = RequestMethod.GET)
	public ModelAndView getImages(@RequestParam Long bookID,HttpServletRequest request) {
		return imageController.getImagesByBookID(bookID, request);
	}
	
	//Category
	@RequestMapping(value= {"/category"}, method = RequestMethod.GET)
	public ModelAndView getCategories(@PathVariable Map<String,String> pathVariable, HttpServletRequest request){
		return categoryController.getCategories(pathVariable, request);
	}
	
	@RequestMapping(value = "/category/addCategory", method = RequestMethod.GET)
	public ModelAndView addCategory() {
		return categoryController.addCategory();
	}
	
	@RequestMapping(value="/category/save", method = RequestMethod.POST)
	public String saveCategory(HttpServletRequest request, @ModelAttribute("cate") Category cate) {
		return categoryController.saveCategory(request, cate);
	}
	
	@RequestMapping (value="/category/getCategory",method = RequestMethod.GET)
	public ModelAndView getCategory(@RequestParam Long categoryID, @RequestParam String mode) {
		return categoryController.getCategory(categoryID, mode);
	}
	
	@RequestMapping(value="/category/delete",method = RequestMethod.GET)
	public String deleteCategory(@RequestParam Long categoryID) {
		return categoryController.deleteCategory(categoryID);
	}
	
}

