package com.website.bookStore.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.website.bookStore.entities.Book;
import com.website.bookStore.serviece.BookServiece;
import com.website.bookStore.serviece.CategoryServiece;

@Controller
@RequestMapping(value="/home")
public class HomeController {
	@Autowired
	BookServiece bookService;
	
	@Autowired 
	CategoryServiece cateService;
	
	@RequestMapping("/")
	public ModelAndView  home(){
		ModelAndView model = new ModelAndView();
		List<Book> bookSale = new ArrayList<>();
		List<Book> bookHot = new ArrayList<>();
		List<Book> bookNew = new ArrayList<>();
		model.setViewName("home");
		model.addObject("categories",cateService.getAll());
		List<Book> listBook = bookService.getAll();
		for(Book book:listBook) {
			do {
				if(book.getStatus().equalsIgnoreCase("Sale") || book.getStatus().equalsIgnoreCase("Deal")) {
					bookSale.add(book);
				}
			}while(bookSale.size()==3);
		}
		
		for(Book book:listBook) {
			do {
				if(book.getStatus().equalsIgnoreCase("Hot")) {
					bookHot.add(book);
				}
			}while(bookHot.size()==4);
		}
		
		for(Book book:listBook) {
			do {
				if(book.getStatus().equalsIgnoreCase("New")) {
					bookNew.add(book);
				}
			}while(bookNew.size()==3);
		}
		
		model.addObject("bookDeal", bookSale);
		model.addObject("bookHot",bookHot);
		model.addObject("bookNew",bookNew);
		return model;
	}
	
}
