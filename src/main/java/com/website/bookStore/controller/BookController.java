package com.website.bookStore.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.website.bookStore.entities.Book;
import com.website.bookStore.entities.Image;
import com.website.bookStore.serviece.BookServiece;
import com.website.bookStore.serviece.CategoryServiece;
import com.website.bookStore.serviece.ImageService;


@Controller
public class BookController {
	
	@Autowired
	private  BookServiece bookService;
	
	@Autowired
	private CategoryServiece cateService;
	
	@Autowired
	private ImageService imageService;
	
	public ModelAndView getBooks(@PathVariable Map<String,String> pathVariable, HttpServletRequest request ) {
		ModelAndView model = new ModelAndView();
		List< Book> books = bookService.getAll();
		for(Book book : books) {
			List<Image> images = book.getImages();
			for(Image image:images) {
				try {
					byte[] encodeBase64 = image.getImageData();
					String base64 = new String(encodeBase64, "UTF-8");
					image.setBase64(base64);
				}
				catch(UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
		model.addObject("books",books);
		model.setViewName("book");
		return model;
	}
	
	public ModelAndView addBook() {
		ModelAndView model = new ModelAndView();
		model.setViewName("formBook");
		model.addObject("categories", cateService.getAll());
		model.addObject("book",new Book());
		return model;
	}
	
	public String saveBook(HttpServletRequest request, @ModelAttribute("book") Book book, @RequestParam CommonsMultipartFile[] imageUpload) {		
		if(book.getBookID() == null) {
			if(imageUpload != null && imageUpload.length > 0) {
				bookService.add(book);
				for(CommonsMultipartFile file : imageUpload) {
					Image image = new Image(file.getBytes(), book);
					imageService.add(image);
				}
			}
			return "redirect:/admin/book/";
		}else {
			bookService.update(book);
			if(imageUpload != null && imageUpload.length > 0) {
				for(CommonsMultipartFile file : imageUpload) {
					Image image = new Image(file.getBytes(), book);
					imageService.add(image);
				}
			}
			return "redirect:/admin/book/";
		}
	}
	
	public ModelAndView getBook(@RequestParam Long bookID, @RequestParam String mode) {
		ModelAndView model = new ModelAndView();
		Book book = bookService.getById(bookID);
		List<Image> images = book.getImages();
		try {
			for(Image image : images) {
				byte[] encodeBase64 = image.getImageData();
				String base64 = new String(encodeBase64, "UTF-8");
				image.setBase64(base64);
			}
		}
		catch(UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		model.setViewName("formBook");
		model.addObject("book", book);
		model.addObject("mode", mode);
		model.addObject("categories",cateService.getAll());
		model.addObject("images",images);
		return model;
	}
	
	public String deleteBook(@RequestParam Long bookID) {
		Book book = bookService.getById(bookID);
		List<Image> images = book.getImages();
		for(Image image : images) {
			imageService.delete(image);
		}
		bookService.delete(book);
		return "redirect:/admin/book/";
	}
	
	
}
