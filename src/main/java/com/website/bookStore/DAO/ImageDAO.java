package com.website.bookStore.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.website.bookStore.entities.Image;

@Repository
public class ImageDAO extends DAO<Image> {
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Image> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Image order by imageID  desc").list();
	}

	@Override
	public Image getById(Long imageid) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Image) session.get(Image.class, new Long(imageid));
		
	}

	@Override
	public Image add(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(image);
		return image;
	}

	@Override
	public Boolean update(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(image);
			return Boolean.TRUE;
		}catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		if(image != null) {
			try {
				session.delete(image);
				return Boolean.TRUE;
			}catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean deleteById(Long imageid) {
		Session session = this.sessionFactory.getCurrentSession();
		Image image = (Image) session.get(Image.class, new Long(imageid));
		if(image != null) {
			try {
				session.delete(image);
				return Boolean.TRUE;
			}catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	
	@SuppressWarnings("unchecked")
	public List<Image> getImageByBookID(Long bookID){
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Image.class);
		criteria.add(Restrictions.like("book.bookID", bookID));
		return criteria.list();
	}
	
}
