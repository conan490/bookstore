package com.website.bookStore.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.website.bookStore.entities.Book;

@Repository
public class bookDAO extends DAO<Book> {
	
	@Autowired
		private SessionFactory sessionFactory;
	
	
	public bookDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public bookDAO() {
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Book> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Book order by bookID desc").list();
	}

	@Override
	public Book getById(Long bookId) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Book) session.get(Book.class, new Long(bookId));
	}

	@Override
	public Book add(Book book) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(book);
		return book;
	}

	@Override
	public Boolean update(Book book) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(book);
			return Boolean.TRUE;
		}catch(Exception e) {
			return Boolean.FALSE;
		}
	}
	@Override
	public Boolean delete(Book book) {
		Session session = this.sessionFactory.getCurrentSession();
		if(book != null) {
			try {
				session.delete(book);
				return Boolean.TRUE;
			}catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean deleteById(Long bookId) {
		Session session = this.sessionFactory.getCurrentSession();
		Book book = (Book) session.get(Book.class, new Long(bookId));
		if(book != null) {
			try {
				session.delete(book);
				return Boolean.TRUE;
			}catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	
}
