package com.website.bookStore.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.website.bookStore.entities.Category;

@Repository
public class categoryDAO extends DAO<Category>{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public categoryDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public categoryDAO() {		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Category order by categoryID desc").list();
	}

	@Override
	public Category getById(Long cateId) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Category) session.get(Category.class, new Long(cateId));
	}

	@Override
	public Category add(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(category);
		return category;
	}

	@Override
	public Boolean update(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(category);
			return Boolean.TRUE;
		}catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		if(category != null) {
			try {
				session.delete(category);
				return Boolean.TRUE;
			}catch(Exception e) {
				return Boolean.FALSE;
			}
			
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean deleteById(Long cateid) {
		Session session = this.sessionFactory.getCurrentSession();
		Category category = (Category) session.get(Category.class, new Long(cateid));
		if(category != null) {
			try {
				session.delete(category);
				return Boolean.TRUE;
			}catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

}
