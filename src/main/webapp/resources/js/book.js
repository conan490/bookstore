function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addBook() {
	location.href='addBook';
}

function getBook(bookID, mode) {
	location.href='getBook?bookID=' + bookID + '&mode=' + mode;
};

function getImages(bookID) {
	location.href='getImages?bookID=' + bookID;
};

function deleteBook(bookID) {
	var r = confirm("Do you want to delete this product?");
	if (r == true) {
		location.href='delete?bookID=' + bookID;
	}	
};