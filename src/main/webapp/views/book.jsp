<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src='<c:url value="/resources/js/bootstrap.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/book.js"></c:url>'></script>
<spring:url value="/resources/css/book.css" var="bookCss"></spring:url>
<link rel="stylesheet" href="${bookCss}">
</head>
<body>
	<div class="panel panel-default">
		<h1><div class="panel-heading text-center">Book List</div></h1>
	</div>
	<div class="panel-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name Book</th>
					<th>Author</th>
					<th>Price</th>
					<th>Describe</th>
					<th>Status</th>
					<th>Category</th>
					<td>Images</td>
					<th>Manipulation</th>
					<%-- <th>Categories</th>--%>
				</tr>
			</thead>
				<c:forEach items="${books}" var="book">
					<tr>
						<td>${book.bookID}</td>
						<td>${book.bookName}</td>
						<td>${book.author}</td>
						<td>${book.price}</td>
						<td>${book.describe}</td>
						<td>${book.status}</td>
						 <td>${book.category.cateName}</td>
						 <td><button class="btn btn-info" onclick="getImages(${book.bookID});">
											<span>Image</span>
								</button></td>
						 <td>
								<button class="btn btn-info" onclick="getBook(${book.bookID},'VIEW');">
											<span class="glyphicon glyphicon-search" title="View"></span>
								</button>
								<button class="btn btn-primary" onclick="getBook(${book.bookID},'EDIT');">
											<span class="glyphicon glyphicon-edit" title="Edit"></span>
								</button>
								<button class="btn btn-danger" onclick="deleteBook(${book.bookID});">
											<span class="glyphicon glyphicon-trash" title="Delete"></span>
								</button>
						</td>
					</tr>
				</c:forEach>
			<tr>
					<th colspan="7" style="text-align:left">
						<button onclick="location.href='addBook'" class="btn btn-primary">
							Add
						</button>
					</th>
			</tr>
		</table>
	</div>
</body>
</html>