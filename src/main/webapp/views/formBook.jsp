<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="generic-container">
		<div class="well lead">Book Form</div>
		<form:form action="./save" method="post" class="form-horizontal" enctype="multipart/form-data" modelAttribute="book">
			<form:input type="hidden" path="bookID" id="bookID" />
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="bookName">Book Name</label>
					<div class="col-sm-7">
						<form:input type="text" path="bookName" id="bookName" class="form-control" 
							name="bookName" placeholder="Input Book Name" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="author">Author</label>
					<div class="col-sm-7">
						<form:input type="text" path="author" id="author" class="form-control" 
							name="author" placeholder="Input Book Author" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="price">Price</label>
					<div class="col-sm-7">
						<form:input type="text" path="price" id="price" class="form-control" 
							name="price" placeholder="Input Book Price" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="describe">Describe</label>
					<div class="col-sm-7">
						<form:input type="text" path="describe" id="describe" class="form-control" 
							name="describe" placeholder="Input Book Describe" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="status">Status</label>
					<div class="col-sm-7">
						<form:input type="text" path="status" id="status" class="form-control" 
							name="status" placeholder="Input Book Status" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<label class="control-label col-sm-3" for="category">Category</label>
				<div class="col-sm-7">
					<form:select path="category.categoryID" id="category" name="category" multiple="true" disabled="${mode == 'VIEW' }">
						<c:forEach items="${categories}" var="category">
								<form:option value="${category.categoryID}">${category.cateName}</form:option>
							</c:forEach>
					</form:select>
				</div>
			</div>
			
			<div class="row">
					<div class="control-label col-sm-3" for="image">Image</div>
					<div class="col-sm-7">
					<c:if test="${mode == 'VIEW'}">
						<c:forEach items="${images}" var="image">
							<img alt="image" src="data:image/jpeg;base64,${image.base64}" width="600px" height="350px">
						</c:forEach>
					</c:if>
					<c:if test="${mode != 'VIEW'}">
								<br>
								<input type="file" class="form-control" id="imageUpload" name="imageUpload" 
									placeholder="Input Image"/>
					</c:if>		
					</div>
				
			</div>
			
			<div class="row">
					<div class="col-sm-offset-4s col-sm-6">
						<c:if test="${mode !='VIEW'}">
							<button type="submit" class="btn btn-primary">
								Save
							</button>
						</c:if>
						<button type="button" onclick="location.href='./'" class="btn btn-default">
							Cancel
						</button>
					</div>
			</div>
		</form:form>
	</div>
</body>
</html>