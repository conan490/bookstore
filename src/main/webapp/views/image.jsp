<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href='<c:url value="/resources/css/bootstrap.min.css"></c:url>'>
<script type="text/javascript" src='<c:url value="/resources/js/jquery.1.10.2.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.min.js"></c:url>'></script>
<link rel="stylesheet" href='<c:url value="/resources/css/image.css"></c:url>'></link>
</head>
<body>
<div class="panel panel-default">
		<h1><div class="panel-heading text-center">Image List</div></h1>
	</div>
	<div class="panel-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Image</th>
				</tr>
			</thead>
			<tr>
				<c:forEach items="${images}" var ="image">
					<td>${image.imageID}</td>
					<td><img alt="image" src="data:image/jpeg;base64,${image.base64}" width="400px" height="250px"></img></td>
				</c:forEach>
			</tr>
		</table>
	</div>
</body>
</html>