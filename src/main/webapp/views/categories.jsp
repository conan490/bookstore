<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<!--<spring:url value="/resources/css/bootstrap.min.css" var="mainCss"></spring:url>
<script type="text/javascript" src='<c:url value="/resources/js/jquery.1.10.2.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.min.js"></c:url>'></script>
<link rel="styleshet" href="${mainCss}">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src='<c:url value="/resources/js/category.js"></c:url>'></script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heading text-center">Category List</div>
	</div>
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name Category</th>
				</tr>
			</thead>
				<c:forEach items="${categories}" var="cate">
					<tr>
						<td>${cate.categoryID}</td>
						<td>${cate.cateName}</td>
						<td>
										<button class="btn btn-info" onclick="getCategory(${cate.categoryID},'VIEW');">
											<span class="glyphicon glyphicon-search" title="View"></span>
										</button>
										<button class="btn btn-primary" onclick="getCategory(${cate.categoryID},'EDIT');">
											<span class="glyphicon glyphicon-edit" title="Edit"></span>
										</button>
										<button class="btn btn-danger" onclick="deleteCategory(${cate.categoryID});">
											<span class="glyphicon glyphicon-trash" title="Delete"></span>
										</button>
						</td>	
					</tr>
				</c:forEach>
			<tr>
					<th colspan="7" style="text-align:left">
						<button onclick="location.href='addCategory'" class="btn btn-primary">
							Add
						</button>
					</th>
			</tr>
			<tr><p><a href ="../">ADMIN</a></p></tr>
		</table>
	</div>
</body>
</html>