<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="generic-container">
		<div class="well lead">Category Information</div>
		<form:form action="./save" method="post" class="form-horizontal" enctype="multipart/form-data" modelAttribute="category">
			<form:input type="hidden" path="categoryID" id="categoryID" />
			
			<div class="row">
				<div class="form-group col-sm-12">
					<label class="control-label col-sm-3" for="cateName">Category Name</label>
					<div class="col-sm-7">
						<form:input type="text" path="cateName" id="cateName" class="form-control" 
							name="cateName" placeholder="Input Category Name" readonly="${mode=='VIEW'}"/>
					</div>
				</div>
			</div>
			
			<div class="row">
					<div class="col-sm-offset-4 col-sm-6">
						<c:if test="${mode !='VIEW'}">
							<button type="submit" class="btn btn-primary">
								Save
							</button>
						</c:if>
						<button type="button" onclick="location.href='./'" class="btn btn-default">
							Cancel
						</button>
					</div>
			</div>
		</form:form>
	</div>
</body>
</html>